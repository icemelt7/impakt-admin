import React from 'react';
import { List, Edit, Create, Datagrid, ReferenceField, TextField, EditButton, DisabledInput, Filter,
  ShowButton, ReferenceInput, SelectInput, SimpleForm, TextInput, Show, SimpleShowLayout } from 'admin-on-rest';

export const PatientShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="name" />
    </SimpleShowLayout>
  </Show>
);

export const PatientList = ({ permissions, ...props }) => (
  <List {...props} filters={<PatientFilter />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <EditButton />
      <ShowButton />
    </Datagrid>
  </List>
);

const PatientTitle = ({ record }) => (
  <span>Patient {record ? `"${record.name}"` : ''}</span>
);

export const PatientFilter = props => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
  </Filter>
);

export const PatientCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
    </SimpleForm>
  </Create>
);

export const PatientEdit = props => (
  <Edit title={<PatientTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
    </SimpleForm>
  </Edit>
);

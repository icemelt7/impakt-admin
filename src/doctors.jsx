import React from 'react';
import { List, Edit, Create, Datagrid, ReferenceField, TextField, EditButton, DisabledInput, Filter,
  ShowButton, ReferenceInput, SelectInput, SimpleForm, TextInput, Show, SimpleShowLayout } from 'admin-on-rest';

export const DoctorShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="name" />
    </SimpleShowLayout>
  </Show>
);

export const DoctorList = props => (
  <List {...props} filters={<DoctorFilter />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <ReferenceField label="Clinic" source="clinicId" reference="clinics">
        <TextField source="name" />
      </ReferenceField>
      <EditButton />
      <ShowButton />
    </Datagrid>
  </List>
);

const DoctorTitle = ({ record }) => (
  <span>Doctor {record ? `"${record.name}"` : ''}</span>
);

export const DoctorFilter = props => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <ReferenceInput label="Clinic" source="clinicId" reference="clinics" allowEmpty>
      <SelectInput optionText="name" />
    </ReferenceInput>
  </Filter>
);

export const DoctorCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <ReferenceInput label="Clinic" source="clinicId" reference="clinics" allowEmpty>
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="name" />
    </SimpleForm>
  </Create>
);

export const DoctorEdit = props => (
  <Edit title={<DoctorTitle />} {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <ReferenceInput label="Clinic" source="clinicId" reference="clinics">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="name" />
    </SimpleForm>
  </Edit>
);

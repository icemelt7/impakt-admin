import React from 'react';
import loopbackRestClient from 'aor-loopback';
import { Admin, Resource, Delete } from 'admin-on-rest';
import Dashboard from './Dashboard';
import authClient from './authClient';

import { DoctorList, DoctorCreate, DoctorEdit, DoctorShow } from './doctors';
import { PatientList, PatientCreate, PatientEdit, PatientShow } from './patients';
import { ClinicList, ClinicCreate, ClinicEdit } from './clinics';
import { ConsultationList, ConsultationCreate, ConsultationEdit, ConsultationShow } from './consultations';

const App = () => (
  <Admin
    dashboard={Dashboard}
    authClient={authClient}
    restClient={loopbackRestClient('http://0.0.0.0:3001/api')}
  >
    {permissions => [
      permissions === 'admin' ? <Resource
        name="doctors"
        show={DoctorShow}
        list={DoctorList}
        edit={DoctorEdit}
        create={DoctorCreate}
        remove={Delete}
      /> : null,
      <Resource
        name="patients"
        show={PatientShow}
        list={PatientList}
        edit={PatientEdit}
        create={PatientCreate}
        remove={Delete}
      />,
      permissions === 'admin' ? <Resource
        name="clinics"
        list={ClinicList}
        edit={ClinicEdit}
        create={ClinicCreate}
        remove={Delete}
      /> : null,
      <Resource
        name="consultations"
        show={ConsultationShow}
        list={ConsultationList}
        edit={ConsultationEdit}
        create={ConsultationCreate}
        remove={Delete}
      />]}
  </Admin>
);

export default App;

import React from 'react';
import { List, Edit, Create, Datagrid, ReferenceField, SelectInput,
  TextField, DateField, EditButton, DisabledInput, DateInput, ReferenceInput, SimpleShowLayout, Show, SimpleForm, TextInput } from 'admin-on-rest';

export const ConsultationList = ({ permissions, ...props }) => (
  <List {...props} filter={permissions === 'doctor' ? { doctorId: 1 } : null}>
    <Datagrid>
      <TextField source="id" />
      <ReferenceField label="Doctor" source="doctorsId" reference="doctors">
        <TextField source="name" />
      </ReferenceField>
      <ReferenceField label="Patient" source="patientsId" reference="patients">
        <TextField source="name" />
      </ReferenceField>
      <DateField source="date" />
      <TextField type="time" source="time" />
      <EditButton />
    </Datagrid>
  </List>
);

export const ConsultationShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <ReferenceField label="Doctor" source="doctorsId" reference="doctors">
        <TextField source="name" />
      </ReferenceField>
      <ReferenceField label="Patient" source="patientsId" reference="patients">
        <TextField source="name" />
      </ReferenceField>
      <DateField source="date" />
      <TextField source="time" />
    </SimpleShowLayout>
  </Show>
);

export const ConsultationCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <ReferenceInput label="Doctor" source="doctorsId" reference="doctors" allowEmpty>
        <SelectInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput label="Patient" source="patientsId" reference="patients" allowEmpty>
        <SelectInput optionText="name" />
      </ReferenceInput>
      <DateInput source="date" />
      <TextInput source="time" type="time" />
    </SimpleForm>
  </Create>
);

export const ConsultationEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <ReferenceInput label="Doctor" source="doctorsId" reference="doctors">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <ReferenceInput label="Patient" source="patientsId" reference="patients">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <DateInput source="date" />
      <TextInput source="time" type="time" />
    </SimpleForm>
  </Edit>
);
